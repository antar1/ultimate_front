$("#btn-add").click(function () {
    var name = $("#inp_name").val();
    var unit = $("#inp_unit").val();
    var quantity = $("#inp_quantity").val();
    var rate = $("#inp_rate").val();

    console.log(name + quantity + unit + rate);

    var payload = {
        "url": 'http://127.0.0.1:5000/addItem',
        "method": 'POST',
        "dataType": 'JSON',
        "data": JSON.stringify({
            'name': name,
            'unit': unit,
            'quantity': quantity,
            'rate': rate
        }),
        headers: {
            "Content-Type": "text/plain",
        }
    };

    console.log(JSON.stringify(payload));

    $.ajax(payload)
        .success(function (result) {
            console.log(JSON.stringify(result));
        }).fail(function () {
            console.log("Failed somehow!!");
            alert("Check internet connection or contact support.");
        }).done(function () {
            clear();
            table.ajax.reload();
        });
});

$("#btn-clear").click(function () {
    clear();
});

var table = $('#datatable').DataTable({
    dom: 'Bfrtip',
    responsive: true,
    // autowidth set to false because if set true, after data load, responsive not working.
    autoWidth: false,
    ajax: function (data, callback, settings) {
        var payload = {
            "url": "http://127.0.0.1:5000/itemList",
            "method": "GET"
        };
        $.ajax(payload)
            .success(function (result) {
                console.log(result);
                callback({ data: JSON.parse(result) });
            }).fail(function () {
                alert("Check internet connection or contact support.");
            });

    },
    columns: [
        {
            "title": "Name",
            "data": "name",
            "targets": 0
        },
        {
            "title": "Unit",
            "data": "unit"
        },
        {
            "title": "Quantity",
            "data": "quantity"
        },
        {
            "title": "Rate",
            "data": "rate"
        }
    ]
});

function clear() {
    $("#inp_name").val("");
    $("#inp_unit").val("");
    $("#inp_quantity").val("");
    $("#inp_rate").val("");
}

// $(document).keydown(function(event){
//     alert(String.fromCharCode(event.which)); 
//   });