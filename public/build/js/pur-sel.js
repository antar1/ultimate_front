var data = {};
var selected_account_id;
$("#calender").datepicker({
    todayHighlight: true,
    autoclose: true,
    format: 'dd M yyyy-D'
});

$("#calender").datepicker('update', new Date());

var table = $('#datatable').DataTable({
    dom: 'Bfrtip',
    "Language": {
        "LoadingRecords": "Please wait - loading..."
    },
    paging: false,
    lengthChange: false,
    searching: true,
    ordering: true,
    // "orderable": true,
    select: true,
    data: null,
    autoWidth: false,
    columns: [
        {
            "title": "Product Name",
            "data": "name"
        },
        {
            "title": "Quantity",
            "data": "quantity",
        },
        {
            "title": "Unit",
            "data": "unit"
        },
        {
            "title": "Rate",
            "data": "rate"
        },
        {
            "title": "Amount",
            "data": "amount"
        },
        {
            "data": "id",
            "visible": false
        }],
    buttons: [
        {
            name: 'add',
            text: 'Add Item',
            className: 'btn-success',
            action: function (e, dt, node, config) {
                $("#btns-add").show();
                $("#btns-modify").hide();
                $("#modal-head-text").text("Add row");
                $("#e_modal").modal("show");
            }
        },
        {
            name: 'modify',
            text: 'Modify',
            enabled: false,
            className: 'btn-warning',
            action: function (e, dt, node, config) {
                $("#btns-add").hide();
                $("#btns-modify").show();
                $("#modal-head-text").text("Modify row");
                $("#e_modal").modal("show");
            }
        },
        {
            name: 'delete',
            text: 'Delete',
            enabled: false,
            className: 'btn-danger',
            action: function (e, dt, node, config) {
                dt.rows({ selected: true }).remove().draw();
            }
        }
    ],
    footerCallback: function (row, data, start, end, display) {
        console.log("Calleddddddddd");
        var api = this.api();
        var total = api.column(4).data().reduce(function (total, current) {
            return parseInt(total) + parseInt(current);
        }, 0);
        console.log(total);
        // Update footer
        $(api.column(4).footer()).html(
            total
        );
    }
});

function change_button_state() {
    var selected = table.rows({ selected: true }).count();
    console.log("selected rows: " + selected);
    if (selected === 0) {
        table.buttons(['modify:name', 'delete:name']).disable();
    }
    else if (selected === 1) {
        var row_data = table.rows({ selected: true }).data()[0];

        $('#inp-name').val(row_data['name']);
        $('#inp-quantity').val(row_data['quantity']);
        $('#inp-rate').val(row_data['rate']);
        $('#inp-amount').val(row_data['amount']);
        table.buttons(['modify:name', 'delete:name']).enable();
    }
    else {
        table.button('delete:name').enable();
        table.button('modify:name').disable();
    }
}

table.on('deselect select draw', function (e, dt, items) {
    change_button_state();
});

$('document').ready(function () {
    $.ajax({
        url: 'http://127.0.0.1:5000/itemList',
        method: 'GET',
        dataType: 'JSON',
        success: function (result) {
            // resultData = result.responseText;
            resultData = result;
            console.log(resultData);
            if (typeof (resultData) === 'string')
                resultData = JSON.parse(resultData);

            console.log(resultData);
            var list = document.getElementById('items');
            resultData.forEach(element => {
                var option = document.createElement('option');
                option.value = element.name;
                // option.text = element.name;
                list.appendChild(option);
            });
            data['item'] = resultData;
        }
    });

    $.ajax({
        url: 'http://127.0.0.1:5000/accountList?need=customer_supplier',
        method: 'GET',
        dataType: 'JSON',
        success: function (result) {
            // resultData = result.responseText;
            resultData = result;
            console.log(resultData);
            if (typeof (resultData) === 'string')
                resultData = JSON.parse(resultData);

            console.log(resultData);
            var list = document.getElementById('party-names');
            resultData.forEach(element => {
                var option = document.createElement('option');
                option.text = element.name;
                list.appendChild(option);
            });
            data['party'] = resultData;
        }
    });
});

$("#inp-name").on('input', function () {
    var val = this.value;
    console.log(val);
    if ($('#items option').filter(function () {
        return this.value === val;
    }).length) {
        data['item'].forEach(element => {
            if (element.name === val) {
                $('#inp-rate').val(element['rate']);
                $('#inp-id').val(element['id']);
            }
        });
    };
});

$("#inp-party-name").on('input', function () {
    var val = this.value;
    console.log(val);
    if ($('#party-names option').filter(function () {
        return this.value === val;
    }).length) {
        data['party'].forEach(element => {
            if (element.name === val)
                $('#balance').val(element['balance'] + ' ' + element['drcr']);
            selected_account_id = element.id;
        });
    };
});

$("#inp-quantity, #inp-rate").on('input', function () {
    console.log($('#inp-quantity').val());
    console.log($('#inp-rate').val());

    var qntt = parseInt($('#inp-quantity').val());
    var rate = parseFloat($('#inp-rate').val());

    $('#inp-amount').val(qntt * rate);
});

$('#btn-add-clr').click(function () {
    add();
    clear();
});

$('#btn-add-cls').click(function () {
    add();
    clear();
    $("#e_modal").modal("hide");
});

$('#btn-save').click(function () {
    console.log("Save clicked!");
    var datas = table.rows().data();
    var request_data = {};
    if (selected_account_id !== undefined)
        request_data.account_id = selected_account_id;
    else
        return;

    request_data.date = $("#calender").datepicker('getFormattedDate', 'dd/mm/yyyy');
    request_data.remarks = "";      //need to be done
    request_data.items = [];

    datas.each(function (row) {
        var item = {}
        item.item_id = row.id;
        item.quantity = row.quantity;
        item.rate = row.rate;
        request_data.items.push(item);
    })

    console.log(JSON.stringify(request_data));

    var payload = {
        url: "http://127.0.0.1:5000/addPurSel",
        method: "POST",
        dataType: "JSON",
        data: JSON.stringify(request_data),
        headers: {
            "Content-Type": "text/plain"
        }
    };
    $.ajax(payload)
        .success(function (result) {
            console.log("in Success");
            console.log(JSON.stringify(result));
        })
        .fail(function (result) {
            console.log("in fail");
            console.log(JSON.stringify(result));
        })
        .done(function (result) {
            console.log("in done");
            console.log(JSON.stringify(result));
        });


});

function add() {
    var name = $('#inp-name').val();
    var quantity = $('#inp-quantity').val();
    var rate = $('#inp-rate').val();
    var amount = $('#inp-amount').val();
    var id = $('#inp-id').val();
    var unit = 'kg';

    console.log(name + " " + quantity + " " + rate + " " + amount);

    if (name.length > 0 && quantity.length > 0 && rate.length > 0 && amount.length > 0)
        table.row.add({
            'name': name,
            'quantity': quantity,
            'unit': unit,
            'rate': rate,
            'amount': amount,
            'id': id
        }).draw();

}
function clear() {
    $('#inp-name').val('');
    $('#inp-quantity').val('');
    $('#inp-rate').val('');
    $('#inp-amount').val('');
    $('#inp-id').val('');
}