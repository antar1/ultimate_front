$("#btn-add").click(function () {
    var name = $("#inp_name").val();
    var address = $("#inp_address").val();
    var contact = $("#inp_contact").val();
    var type = $("input[name=inp_type]:checked").val();

    console.log(name + address + contact + type);

    var payload = {
        "url": 'http://127.0.0.1:5000/addAccount',
        "method": 'POST',
        "dataType": 'JSON',
        "data": JSON.stringify({
            'name': name,
            'address': address,
            'contact': contact,
            'type': type
        }),
        headers: {
            "Content-Type": "text/plain",
        }
    };

    console.log(JSON.stringify(payload));

    $.ajax(payload)
        .success(function (result) {
            console.log(JSON.stringify(result));
        }).fail(function () {
            console.log("Failed somehow!!");
            alert("Check internet connection or contact support.");
        }).done(function () {
            table.ajax.reload();
        });
});

$("#btn-clear").click(function () {
    clear();
});

var table = $('#datatable').DataTable({
    dom: 'Bfrtip',
    responsive: true,
    // autowidth set to false because if set true, after data load, responsive not working.
    autoWidth: false,
    ajax: function (data, callback, settings) {
        var payload = {
            "url": "http://127.0.0.1:5000/accountList",
            "method": "GET"
        };
        $.ajax(payload)
            .success(function (result) {
                console.log(result);
                callback({ data: JSON.parse(result) });
            }).fail(function () {
                alert("Check internet connection or contact support.");
            });

    },
    columns: [
        {
            "title": "Name",
            "data": "name",
            "targets": 0
        },
        {
            "title": "Type",
            "data": "type"
        },
        {
            "title": "Address",
            "data": "address"
        },
        {
            "title": "Contact",
            "data": "contact"
        }
    ]
});

function clear() {
    $("#inp_name").val("");
    $("#inp_address").val("");
    $("#inp_contact").val("");
    $("#inp_type_1").prop("checked", true);
}

// $(document).keydown(function(event){
//     alert(String.fromCharCode(event.which)); 
//   });