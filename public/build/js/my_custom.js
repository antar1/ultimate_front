var tasks = { 'page_load': false, 'ajax_complete': true }
function toggle_loading() {
    if (tasks.ajax_complete === true && tasks.page_load === true)
        $('#loading_div').hide();
    else
        $('#loading_div').show();
}
$(document).ready(function () {
    toggle_loading();
});

$(window).load(function () {
    tasks.page_load = true;
    toggle_loading();
});

$(document).ajaxComplete(function () {
    tasks.ajax_complete = true;
    console.log("in complete");
    toggle_loading();
});

$(document).ajaxSend(function () {
    tasks.ajax_complete = false;
    console.log("in send");
    toggle_loading();
});
