"use strict";
var express = require('express');
var app = express();

app.set('view engine', 'ejs');

app.use(express.static('public'));
var favicon = require('serve-favicon');

app.use(favicon(__dirname + '/public/images/favicon.ico'));

app.get('/ledger', function (req, res) {
  console.log('Rendering ledger page');
  res.render('pages/pur-sel');
});

app.get('/receipt', function (req, res) {
  console.log('Rendering ledger page');
  res.render('pages/receipt');
});

app.get('/party', function (req, res) {
  console.log('Rendering party page');
  res.render('pages/party');
});

app.get('/product', function (req, res) {
  console.log('Rendering product page');
  res.render('pages/product');
});

app.get('/', function (req, res) {
  console.log('Rendering home page');
  res.render('pages/home');
});
app.get('')

app.listen(3000, '127.0.0.1', function () {
  console.log('Running on port 3000...');
});